import java.io.*;
import java.net.*;
import java.util.*;
import java.lang.*;

public class CachingHTTPClient 
{
	public static void main(String args[]) 
	{
		if (args.length < 1) 
		{
			System.out.println("Usage:");
			System.out.println("java TestUrlConnection <url>");
			System.exit(0);
		}

		URL url = null;
		Map<String, Map<String, String>> cachedData = new HashMap<String, Map<String, String>>();
		try 
		{
			url = new URL(args[0]);
		} catch (MalformedURLException e) {
			e.printStackTrace();
			System.out.println("Don't forget to include \"http://\" protocol");
			System.exit(0);
		}
		
		assert url != null;

		try
		{
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			File directory = new File("/tmp/jem4372/assignment1/");
			File file = new File("/tmp/jem4372/assignment1/cache.ser");

			if (file.exists())
			{
				getResponse(url, cachedData, connection, directory, file);
			}

			else 
			{
				directory.mkdirs();
				file.createNewFile();

				getCache(url, cachedData, connection, directory, file);
			}	
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public static void getResponse (URL url, Map<String, Map<String, String>> cachedData, HttpURLConnection connection, File directory, File file)
	{
		try
		{
			FileInputStream fileIn = new FileInputStream("/tmp/jem4372/assignment1/cache.ser");
			ObjectInputStream in = new ObjectInputStream(fileIn);
			cachedData = (Map<String, Map<String, String>>) in.readObject();
			in.close();
			fileIn.close();

			if (cachedData.get(url.toString()) != null)
			{
				Map<String, String> urlResponse = cachedData.get(url.toString());

				if (urlResponse.get("max-age") != null && Integer.valueOf(urlResponse.get("max-age")) > 0) 
				{
					System.out.println("***** Serving from the cache – start *****");
					System.out.println("Date = " + urlResponse.get("Date"));
					System.out.println("Expires = " + urlResponse.get("Expires"));
					System.out.println("max-age = " + urlResponse.get("max-age"));
					System.out.println("Response Code = " + urlResponse.get("responseCode"));
					System.out.println(urlResponse.get("responseBody"));
					System.out.println("***** Serving from the cache – end *****");
				}

				else if (urlResponse.get("Expires") != null )
				{
					System.out.println("***** Serving from the cache – start *****");
					System.out.println("Date = " + urlResponse.get("Date"));
					System.out.println("Expires = " + urlResponse.get("Expires"));
					System.out.println("max-age = " + urlResponse.get("max-age"));
					System.out.println("Response Code = " + urlResponse.get("responseCode"));
					System.out.println(urlResponse.get("responseBody"));
					System.out.println("***** Serving from the cache – end *****");
				}

				else 
				{
					System.out.println("***** Serving from the source – start *****");
					System.out.println("Date:" + connection.getHeaderField("Date"));
					System.out.println("Expires:" + connection.getHeaderField("Expires"));
					System.out.println("Cache-Control:" + connection.getHeaderField("Cache-Control"));
					System.out.println("Connection:" + connection.getHeaderField("Connection"));

					InputStream input = connection.getInputStream();
					StringBuilder sb = new StringBuilder();
					Scanner scan = new Scanner(input);

					while (scan.hasNext())
					{
						sb.append(scan.next());
						sb.append("");
					}
					urlResponse.put("responseBody", sb.toString());
					System.out.println(urlResponse.get("responseBody"));
					System.out.println("***** Serving from the source – end *****");
				}
			}

			else 
			{
				getCache(url, cachedData, connection, directory, file);
			}
		} catch(IOException e) {
			e.printStackTrace();
		} catch(ClassNotFoundException c) {
			System.out.println("cache not found");
			c.printStackTrace();
		}
	}

	public static void getCache (URL url, Map<String, Map<String, String>> cachedData, HttpURLConnection connection, File directory, File file)
	{
		try
		{
			FileOutputStream fileOut = new FileOutputStream("/tmp/jem4372/assignment1/cache.ser");
			ObjectOutputStream out = new ObjectOutputStream(fileOut);

			try 
			{
				Map<String, String> headerFields = new HashMap<String, String>();


				headerFields.put("Expires", connection.getHeaderField("Expires"));

				String maxAge = null;
				String cacheControl = connection.getHeaderField("Cache-Control");
				if (cacheControl != null)
				{
					List<String> cacheControlList = Arrays.asList(cacheControl.split("\\s*,\\s*"));

					Iterator<String> cacheControlIter = cacheControlList.iterator();
					while (cacheControlIter.hasNext())
					{
						String cacheControlHeader = cacheControlIter.next();
						if (cacheControlHeader.contains("max-age=")) {
							maxAge = cacheControlHeader.substring(8);
						}
					}
					headerFields.put("max-age", maxAge);

				}

				headerFields.put("responseCode", "" + connection.getResponseCode() + "");
				headerFields.put("Date", connection.getHeaderField("Date"));

				InputStream input = connection.getInputStream();
				StringBuilder sb = new StringBuilder();
				Scanner scan = new Scanner(input);
				while (scan.hasNext())
				{
					sb.append(scan.next());
					sb.append("");
				}
				headerFields.put("responseBody", sb.toString());


				connection.disconnect();
		  		// connection.setRequestProperty("If-Modified-Since", headerFields.get("Expires"));
				// System.out.println(connection.getIfModifiedSince());
				// System.out.println(connection.getLastModified());

				System.out.println("***** Serving from the source – start *****");
				System.out.println("Date = " + headerFields.get("Date"));
				System.out.println("Expires = " + headerFields.get("Expires"));
				System.out.println("max-age = " + headerFields.get("max-age"));
				System.out.println("Response Code = " + headerFields.get("responseCode"));
				System.out.println(headerFields.get("responseBody"));
				System.out.println("***** Serving from the source – end *****");

				cachedData.put(url.toString(), headerFields);

				out.writeObject(cachedData);
				out.flush();
				out.close();
				fileOut.close();		

			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch(IOException e) {
			e.printStackTrace();
		}
	}
}
